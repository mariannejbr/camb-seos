from matplotlib.ticker import AutoMinorLocator
from scipy.interpolate import UnivariateSpline
import pylab as plt
import numpy as np
import math

# constantes fisicas & numericas
pi = math.pi
c = 299792458.
hbar = 6.62606896/(2*pi)*1e-34
e = 1.6021773*1e-19
G = 6.6738*1e-11
Mpc = 3.085678*1e22
kB = 1.3806504*1e-23
Mpl= math.sqrt(hbar*c/(8*pi*G))*c**2/e        #masa reducida de Planck en eV
c8piGeVm2 = 16*pi**2*G*e**2/(2*pi*hbar*c**5)  #valor de 8piG en eV^-2
eV2toMpcm2 = (2*pi*e*Mpc/(c*2*pi*hbar))**2    #conversion: 1eV^2 = (eV2toMpcm2)*Mpc^-2

# temperatura del CMB (en K) y otros parametros
Tg0 = 2.7255
Nnu = 3.046
omegabh2 = 0.02251066
omegach2 = 0.1176474
h = 0.6787461
aini = 1.e-9
a0 = 1.

# densidades al dia de hoy (en Mpc^-4); grhox0 tiene unidades de Mpc^-2
rhocr0 = 3*1.e10*h**2*eV2toMpcm2/(c**2*c8piGeVm2)
rhog0  = pi**2/15.*Tg0**4*(kB/e)**4*eV2toMpcm2**2
rhon0  = 7./8.*(4./11.)**(4./3.)*Nnu*rhog0
rhor0  = rhog0+rhon0
rhom0  = (omegabh2 + omegach2)*rhocr0/(h**2)
rhox0  = rhocr0 - rhom0 - rhor0
grhox0 = 3*1.e10/(c**2)*(rhox0/rhocr0)*h**2
#print rhocr0, rhog0, rhon0, rhor0, rhom0, rhox0, grhox0

# parametros de la sEoS
w0 = -1.
wa = -0.18
zT = 1.
q  = 1.

# arreglos con la informacion
N = []
a = []
w = []
rhor  = []
rhom  = []
rhox  = []
grhox = []

# primero llenamos los arreglos N & fN = 3*(1+w(N))
n = 1000
Nini = math.log(aini/aini)
N0 = math.log(a0/aini)
dN = N0/(n-1)
fN = []

for i in range(0,n+2):
    Nend = i*dN
    N.append(Nend)
    aend = aini*math.exp(Nend)
    a.append(aend)
    integrando = 3*(1.+w0 + wa*(1.-aend)**q/((aend*zT)**q+(1-aend)**q))
    fN.append(integrando)

# aqui hacemos la interpolacion del integrando fN. La mejor coincidencia respecto a mathematica, se da cuando s = 0
interp = UnivariateSpline(N,fN,s=0)

for j in range(0,n+2):
    Nend = N[j]
    aend = a[j]
    wend = w0 + wa*(1.-aend)**q/((aend*zT)**q+(1-aend)**q)
    w.append(wend)
    rhorend = rhor0/(aend**4)
    rhor.append(rhorend)
    rhomend = rhom0/(aend**3)
    rhom.append(rhomend)
    rhoxend = rhox0*math.exp(interp.integral(Nend,N0))
    rhox.append(rhoxend)
    grhoxend = c8piGeVm2/eV2toMpcm2*aend**4*rhoxend
    grhox.append(grhoxend)
    #print N[j], a[j], w[j], rhor[j], rhom[j], rhox[j], grhox[j]

# comparacion de los resultados de mathematica con los de python
mathematica = np.loadtxt('/home/ealmaraz/software/camb/seos/v1/analysis/seosv1_mathematica.dat')
plt.plot(mathematica[:,0],mathematica[:,1],'--r',label='$\\rho_{r}$ - mathematica',linewidth=1)
plt.plot(a,rhor,'-r',label='$\\rho_{r}$ - python',linewidth=1)
plt.plot(mathematica[:,0],mathematica[:,2],'--k',label='$\\rho_{m}$ - mathematica',linewidth=1)
plt.plot(a,rhom,'-k',label='$\\rho_{m}$ - python',linewidth=1)
plt.plot(mathematica[:,0],mathematica[:,3],'--b',label='$\\rho_{DE}$ - mathematica',linewidth=1)
plt.plot(a,rhox,'-b',label='$\\rho_{DE}$ - python',linewidth=1)
plt.xscale('log')
plt.xlim([1e-9,1])
plt.xlabel('$a$',fontsize=15)
plt.yscale('log')
plt.ylabel('$\\rho$ (Mpc$^{-4}$)',fontsize=15)
plt.legend(loc= 'best', frameon=False)
plt.show()






