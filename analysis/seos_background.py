from scipy.interpolate import UnivariateSpline
from scipy import interpolate
import pylab as plt
import numpy as np
from pylab import *
import math

#archivos con los datos
seos_bk = np.loadtxt('/home/ealmaraz/software/camb/seos/v1/seosv1_background.txt')


fig = plt.figure()
fig.suptitle('evolucion de $w_{\mathrm{DE}}$')

plt.plot(seos_bk[:,2],seos_bk[:,9],'-k',linewidth=1,label='best fit Mariana')

plt.xlabel('$z$',fontsize=20)
plt.xlim([0,3])
plt.xticks(fontsize=15)
#plt.xscale('log')
plt.ylabel('$w_{\mathrm{DE}}$',fontsize=20)
plt.ylim([-1.1,-0.91])
plt.grid(True)
plt.legend(loc='best')

#plt.savefig('.png')
plt.show()

