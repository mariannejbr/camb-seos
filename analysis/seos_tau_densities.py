## This script contains the definition of "seos" equation of state
## and the calculation of the volumetric density (rho) of the different
## components for a flat universe with the cosmology provided  by
## Planck 2015 Cosmological Parameters
## report (arxiv:1502.01589v2) table 3 column 4:
## "Planck + TT + TE + EE + low P".

from scipy import integrate, exp, sqrt
from Constants_Units import *
import plotly as py
from plotly.tools import FigureFactory as FF

QUAD_EPSABS = 1.49e-8
inf = np.inf
quad = integrate.quad


# ---------------------------------------------------------------------#
# This part of the script contains the definition of a new equation
# of state for DE component:
# w(z) = w0 + (wi-w0) (z/zt)^q/(1+(z/zt)^q)

def wz(z, w0=-1, wi=-1, q=1, zt=1):
    """This function parametrizes the eos for DE at low redshifts"""
    if zt == 0:
        return wi
    if q == 1:
        return w0 + (wi - w0) * ((z / zt) / (1 + (z / zt)))
    return w0 + (wi - w0) * ((z / zt) ** q / (1 + (z / zt) ** q))


def f_DEz_integrand(zp, w0, wi, q, zt):
    return (1 + wz(zp, w0, wi, q, zt)) / (1 + zp)


def f_DEz(z, w0=-1, wi=-1, q=1, zt=1):
    """Integral of DE eos for Hubble function"""
    intDE, error = integrate.quad(f_DEz_integrand, 0, z,
                                  epsabs=QUAD_EPSABS,
                                  args=(w0, wi, q, zt))
    return exp(3 * intDE)


def f_DElna_integrand(u, q, zt):
    """Integrand in terms of u=lna variable"""
    return 1 / ((1 + ((zt * exp(u)) / (1 - exp(u))) ** q))


def f_DEa(avalue, w0=-1, wi=-1, q=1, zt=1):
    """
    integral of DE eos for Hubble function
    in terms of scale factor
    split in two terms: one analitically solved and a numerical integration
    """
    if avalue == 1:
        return 1
    intDEa, error = integrate.quad(f_DElna_integrand, 0, np.log(avalue),
                                   epsabs=QUAD_EPSABS,
                                   args=(q, zt))
    wa = wi - w0
    return 1 / (avalue ** (1 + w0)) * exp(-3 * wa * intDEa)


# ------############ HUBBLE FUNCTIONS ################-----------#

def hubbleflat(z, w0=-1, wi=-1, q=1, zt=1, h=REDUCED_H0, OmegaM=OMEGAM0):
    """
    Hubble function in terms of z for a flat universe
    :: free params: OmegaM, h, w0, wi, q, zt
    """
    hubblefunc = H0p * h * sqrt(
        OMEGAR0 * (1 + z) ** 4 + OmegaM * (1 + z) ** 3 +
        (1 - OMEGAR0 - OmegaM) * f_DEz(z, w0, wi, q, zt))
    return hubblefunc


def hubbleflata0(a, w0=-1, wi=-1, q=1, zt=1, h=REDUCED_H0, OmegaM=OMEGAM0):
    """
        Hubble function in terms of scale factor a for a flat universe
        :: free params: OmegaM, h, w0, wi, q, zt
        """
    hubblefunc = H0p * h * sqrt(
        (OMEGAR0) / (a ** 4) + (OmegaM) / (a ** 3) +
        (1 - OMEGAR0 - OmegaM) * f_DEa(a, w0, wi, q, zt))
    return hubblefunc


##### ---------------------------------------------------------------#####

def rhode(z, w0, wi, q, zt, OmegaM):
    """
    volumentric energy density of the dark energy fluid component
    for a flat universe in terms of OmegaMatter as free parameter
    and the parameters of "seos"
    as function of redshift
    """
    rhode0 = (1 - OmegaM - OMEGAR0) * RHOCR0
    return rhode0 * f_DEz(z, w0, wi, q, zt)


def rhodea0(avalue, w0, wi, q, zt, OmegaM):
    """
    volumentric energy density of the dark energy fluid component
    for a flat universe in terms of OmegaMatter as free parameter
    and the parameters of "seos"
    as function of scale factor
    """
    rhode0 = (1 - OmegaM - OMEGAR0) * RHOCR0
    return rhode0 * f_DEa(avalue, w0, wi, q, zt)


def rhomz(z, OmegaM):
    """
    volumentric energy density of the matter component
    for a flat universe in terms of OmegaMatter as free parameter
    as function of redshift
    """
    rhom0 = OmegaM * RHOCR0
    return rhom0 * (1 + z) ** 3


def rhoma0(avalue, OmegaM):
    """
    volumentric energy density of the matter component
    for a flat universe in terms of OmegaMatter as free parameter
    as function of scale factor
    """
    rhom0 = OmegaM * RHOCR0
    return rhom0 / avalue ** 3


# # # ------- Cosmic distances derived from the Hubble function-------####

def tau_integrand(aprime, w0, wi, q, zt, h=REDUCED_H0, OmegaM=OMEGAM0):
    """
    integrand 1/(a²H(a)) for calculation of conformal time
    """
    return 1 / (aprime ** 2 * hubbleflata0(aprime, w0, wi, q, zt, h, OmegaM))


def tau_conf_time(a, w0, wi, q, zt, h, OmegaM):
    """
    conformal time at given value of a(t)
    calculated as tau = int_0^a da/(a^2H) for a flat Universe
    """
    tau, error = integrate.quad(tau_integrand, 0, a,
                                epsabs=QUAD_EPSABS,
                                args=(w0, wi, q, zt, h, OmegaM))
    return tau


## -----------------------------------------------------------------------##
##
## let's build a table with the results
##

# astart, aend = 1e-9, 1
# avalues = np.arange(astart, aend, 0.001)

# zend = 1 / astart - 1
# z0 = 0
# #zvalues = np.arange(z0, zend, 0.01)

# LCDM = (-1, -1, 1, 1, OMEGAM0)

# for i in avalues:
#     matrix = np.array([[],[],[]])
#     data_matrix = matrix.append([[i, rhodea0(i, *LCDM), rhoma0(i, OMEGAM0)]])
# print(data_matrix)
#np.savetxt('rho-s.txt', data_matrix)

#from pickle import dump

# with open('RHOS.data', 'wb') as fp:
#    dump(table, fp)

# ("RHOs.txt", table)
# print(table)
